#include <dht.h>

// DHT - Version: Latest 
#include <dht.h>

#include "DHT.h"

#define DHTPIN 9     // what digital pin we're connected to

// Link for app: https://play.google.com/store/apps/details?id=appinventor.ai_addmefaster1.BlueCore_Tech_Temp_Humidity
// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);
int analogPin=0;
int val=0;
void air(){
  val=analogRead(analogPin);
  float p=((val/1023)*100);
  if (p<=17){
    Serial.println("Sensor Reading: ");
    Serial.println(val);
    Serial.println("Air is Clean with little pollutants");
  }
  else if (17<p<40)
  {
    Serial.println("Sensor Reading: ");
    Serial.println(val);
    Serial.println("Air is Polluted.");
  }
  else {
    Serial.println("Sensor Reading: ");
    Serial.println(val);
    Serial.println("Air is Very Polluted! Not Good for your Health");
 
  }
}
void setup() {
  Serial.begin(9600);
  Serial.println("Vimarsh Shah Temperature and Humidity");

  dht.begin();
}

void loop() {
  // Give it time to calibrate
  delay(20000);
  
  float h = dht.readHumidity();
  // Read Celsius
  float t = dht.readTemperature();
  // Read Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check errors
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.println("");
  //Serial.println("Vimarsh Shah Temperature and Humidity Device");
  Serial.println("");
  Serial.println("Humidity: ");
  Serial.print(h);
  Serial.println(" %.");
  Serial.println("  ");
  Serial.println("Temperature: ");
  Serial.print(t);
  Serial.println(" Degrees ");
  air();
}
