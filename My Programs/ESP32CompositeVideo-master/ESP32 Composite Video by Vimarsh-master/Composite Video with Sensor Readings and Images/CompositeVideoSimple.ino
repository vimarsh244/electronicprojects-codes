#include "DHT.h"

#define DHTPIN 18     // what digital pin we're connected to

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
float humidity;
float temperature;

DHT dht(DHTPIN, DHTTYPE);
#define flame 19
#define pir 21
#define mq135 4
#define mq7 15

int flame_readings;
int pir_readings;
float mq7_readings;
float mq135_readings;

#include "CompositeGraphics.h"
#include "Image.h"
#include "CompositeOutput.h"

//#include "luni.h"
#include "font6x8.h"
#include "vlogo1.h"
//#include "vlogo2.h"
#include "vlogo3.h"
#include "vlogo4.h"

//PAL MAX, half: 324x268 full: 648x536
//NTSC MAX, half: 324x224 full: 648x448
const int XRES = 320;
const int YRES = 200;

//Graphics using the defined resolution for the backbuffer
CompositeGraphics graphics(XRES, YRES);
//Composite output using the desired mode (PAL/NTSC) and twice the resolution.
//It will center the displayed image automatically
CompositeOutput composite(CompositeOutput::NTSC, XRES * 2, YRES * 2);

//image and font from the included headers created by the converter. Each iamge uses its own namespace.
Image<CompositeGraphics> vlogo10(vlogo1::xres, vlogo1::yres, vlogo1::pixels);
//Image<CompositeGraphics> vlogo20(vlogo2::xres, vlogo2::yres, vlogo2::pixels);
Image<CompositeGraphics> vlogo30(vlogo3::xres, vlogo3::yres, vlogo3::pixels);
Image<CompositeGraphics> vlogo40(vlogo4::xres, vlogo4::yres, vlogo4::pixels);
//font is based on ASCII starting from char 32 (space), width end height of the monospace characters.
//All characters are staored in an image vertically. Value 0 is background.
Font<CompositeGraphics> font(6, 8, font6x8::pixels);

#include <soc/rtc.h>

void setup()
{
  //highest clockspeed needed
  rtc_clk_cpu_freq_set(RTC_CPU_FREQ_240M);

  //initializing DMA buffers and I2S
  composite.init();
  //initializing graphics double buffer
  graphics.init();
  //select font
  graphics.setFont(font);
  dht.begin();
  pinMode(flame, INPUT);
  pinMode(pir, INPUT);
  pinMode(mq7, INPUT);
  pinMode(mq135, INPUT);
  //running composite output pinned to first core
  xTaskCreatePinnedToCore(compositeCore, "c", 1024, NULL, 1, NULL, 0);
  //rendering the actual graphics in the main loop is done on the second core by default
}

void compositeCore(void *data)
{
  while (true)
  {
    //just send the graphics frontbuffer whithout any interruption
    composite.sendFrameHalfResolution(&graphics.frame);

  }
}

void draw()
{
  //clearing background and starting to draw
  graphics.begin(0);
  //drawing an image
  vlogo10.draw(graphics, 20, 70);
  //vlogo20.draw(graphics, 0, 10);
  vlogo30.draw(graphics, 160, 70);
  vlogo40.draw(graphics, 160, 130);

  mq7_readings = analogRead(mq7);
  mq135_readings = analogRead(mq135);

  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
  if (isnan(humidity) || isnan(temperature)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  flame_readings = !digitalRead(flame);

  pir_readings = digitalRead(pir);

  //drawing a frame
  // graphics.fillRect(27, 18, 160, 30, 10);
  //graphics.rect(10, 10, 160, 300, 50);

  //setting text color, transparent background
  graphics.setTextColor(50);
  //text starting position
  graphics.setCursor(20, 10);
  //printing some lines of text
  graphics.print("Hello!");
  graphics.print(" free flash memory in ESP32: ");
  graphics.print((int)heap_caps_get_free_size(MALLOC_CAP_DEFAULT));
  graphics.print("\n This Program is designed by Vimarsh to give out   sensor readings on composite video...");
  graphics.print("\n\n Temperature: ");
  graphics.print(temperature);
  graphics.print(" oC \t\t||\t\t Humidity: ");
  graphics.print(humidity);
  graphics.print("%");
  graphics.print("\n Fire Raw: ");
  graphics.print((flame_readings));
  graphics.print("\t\t||\t\t Motion Raw: ");
  graphics.print((pir_readings));
  graphics.print("\n MQ7 Raw: ");
  graphics.print(mq7_readings);
  graphics.print("\t\t||\t\t MQ135 Raw: ");
  graphics.print(mq135_readings);
  /*
    //drawing some lines
    for(int i = 0; i <= 100; i++)
    {
      graphics.line(50, i + 60, 50 + i, 160, i / 2);
      graphics.line(150, 160 - i, 50 + i, 60, i / 2);
    }

    //draw single pixel
    graphics.dot(20, 190, 10);
  */
  //finished drawing, swap back and front buffer to display it
  graphics.end();
}

void loop()
{
  draw();
}


