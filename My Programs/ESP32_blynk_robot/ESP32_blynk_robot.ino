/*

   Blynk app controlled Robot with Horbill ESP32.
    For more details visit:
    https://exploreembedded.com/wiki/Robo_with_Hornbill_ESP32


*/

#define BLYNK_PRINT Serial    // Comment this out to disable prints and save space
#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#define m11 5
#define m12 4
#define m21 0
#define m22 14
//pins to drive motors


// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "59faea20344a41879099a33da18165de";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "ESP@vimarsh";
char pass[] = "password@ESP";


void setup()
{
  Serial.begin(115200);
  Blynk.begin(auth, ssid, pass);
  pinMode(m11, OUTPUT);
  pinMode(m12, OUTPUT);
  pinMode(m21, OUTPUT);
  pinMode(m22, OUTPUT);
  //Serial.print("*Explore Robo Mode Computer: Controlled*\n\r");
  //Serial.println("Commands:\n W->Forward \n S->Backwards \n A->Left \n D->Right");
}

void loop()
{

  Blynk.run();

}
BLYNK_WRITE(V1)
{
  int x = param[0].asInt();
  int y = param[1].asInt();

  // Do something with x and y
  /*  Serial.print("X = ");
    Serial.print(x);
    Serial.print("; Y = ");
    Serial.println(y);*/
  if (y > 220)
    right();
  else if (y < 35)
    left();
  else if (x > 220)
    backward();
  else if (x < 35)
    forward();
  else
    Stop();
}
void forward()
{
  digitalWrite(m11, HIGH);
  digitalWrite(m12, LOW);
  digitalWrite(m21, HIGH);
  digitalWrite(m22, LOW);
}

void backward()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12, HIGH);
  digitalWrite(m21, LOW);
  digitalWrite(m22, HIGH);
}

void right()
{
  digitalWrite(m11, HIGH);
  digitalWrite(m12, LOW);
  digitalWrite(m21, LOW);
  digitalWrite(m22, HIGH);
}

void left()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12, HIGH);
  digitalWrite(m21, HIGH);
  digitalWrite(m22, LOW);
}

void Stop()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12, LOW);
  digitalWrite(m21, LOW);
  digitalWrite(m22, LOW);
}
