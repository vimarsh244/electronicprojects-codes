#define BLYNK_PRINT Serial
#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

#define m11 27
#define m12 14
#define m21 12
#define m22 13
//pins to drive motors


#include "DHT.h"
#define DHTPIN 32     // what digital pin dht is connected to
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)
DHT dht(DHTPIN, DHTTYPE);
//for DHT sensor

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
//libraries for bmp280
Adafruit_BMP280 bmp; // I2C
//Adafruit_BMP280 bmp(BMP_CS); // hardware SPI
//Adafruit_BMP280 bmp(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);

//other sensor pins
#define flame 22
#define pir 21
#define raindrop 23
#define mq2 33

//variables
long gas;//for mq2
int flame_read, pir_read, raindrop_read;
int h, t, p;   //for temp,humid for dht //for press for bmp

char auth[] = "e0ac813b3b5c4eb9b4cbea91693c42bd";
char ssid[] = "Vardhman_2.4GHz";
char pass[] = "V$9825067189$";

BlynkTimer timer;


void senssend()
{
  pir_read = digitalRead(pir);
  raindrop_read = !digitalRead(raindrop);
  flame_read = !digitalRead(flame);
  Blynk.virtualWrite(V15, pir_read);
  Blynk.virtualWrite(V16, raindrop_read);
  Blynk.virtualWrite(V17, flame_read);
 h = dht.readHumidity();
   t = dht.readTemperature();
   p = (bmp.readPressure() / (0.00750062));
  gas = analogRead(mq2);
  Serial.println(t);

  Serial.println(h);
  Serial.println(p);

  Blynk.virtualWrite(V21, t);
  Blynk.virtualWrite(V22, h);
  Blynk.virtualWrite(V23, p);
  Blynk.virtualWrite(V24, gas);
}
void setup()
{
  Serial.begin(115200);
  Blynk.begin(auth, ssid, pass);
  pinMode(m11, OUTPUT);
  pinMode(m12, OUTPUT);
  pinMode(m21, OUTPUT);
  pinMode(m22, OUTPUT);
  Blynk.email("SmartAgriculture Robot Update", "The robot project is online.");
  timer.setInterval(2000L, senssend);
  dht.begin();
}

void loop()
{

  Blynk.run();
  timer.run();
}
BLYNK_WRITE(V1)
{
  int x = param[0].asInt();
  int y = param[1].asInt();
  if (y > 220)
    right();
  else if (y < 35)
    left();
  else if (x > 220)
    backward();
  else if (x < 35)
    forward();
  else
    Stop();
}
void forward()
{
  digitalWrite(m11, HIGH);
  digitalWrite(m12, LOW);
  digitalWrite(m21, HIGH);
  digitalWrite(m22, LOW);
}

void backward()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12, HIGH);
  digitalWrite(m21, LOW);
  digitalWrite(m22, HIGH);
}

void right()
{
  digitalWrite(m11, HIGH);
  digitalWrite(m12, LOW);
  digitalWrite(m21, LOW);
  digitalWrite(m22, HIGH);
}

void left()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12, HIGH);
  digitalWrite(m21, HIGH);
  digitalWrite(m22, LOW);
}

void Stop()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12, LOW);
  digitalWrite(m21, LOW);
  digitalWrite(m22, LOW);
}

