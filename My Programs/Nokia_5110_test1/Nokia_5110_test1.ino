#include <LCD5110_Graph.h>

#include "DHT.h"

#define DHTPIN 2     // what digital pin we're connected to

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
LCD5110 lcd(8,9,10,12,11);
DHT dht(DHTPIN, DHTTYPE);

extern uint8_t icons[];
extern uint8_t pi[];
extern uint8_t cube[];
extern uint8_t vshah[];
extern uint8_t works[];
extern uint8_t intro[];
extern unsigned char SmallFont[];
extern unsigned char TinyFont[];

int LDRpin = 13;
char lightString [8];
String str; 

int lightIntensity = 0;
void setup()
{
  Serial.begin(9600);
  Serial.println("DHTxx test!");

  dht.begin();
  lcd.InitLCD();
    lcd.setFont(SmallFont);
  randomSeed(analogRead(7));
}

void loop()
{
  lcd.clrScr();
  lcd.drawBitmap(0,0,intro,84,48);
  lcd.update();
  delay(2000);
  
  lcd.clrScr();
  lcd.drawBitmap(0,0,vshah,84,48);
  lcd.update();
  delay(2000);
  
  lcd.clrScr();
  lcd.drawBitmap(0,0,works,84,48);
  lcd.update();
  delay(2000);
  
  lcd.clrScr();
  lcd.drawBitmap(0,0,pi,84,48);
  lcd.update();
  delay(2000);

  lcd.clrScr();
  lcd.drawBitmap(0,0,icons,84,48);
  lcd.update();
  delay(2000);
  
  lcd.clrScr();
  lcd.drawBitmap(0,0,cube,84,48);
  lcd.update();
  delay(2000);

  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.print(f);
  Serial.print(" *F\t");
  Serial.print("Heat index: ");
  Serial.print(hic);
  Serial.print(" *C ");
  Serial.print(hif);
  Serial.println(" *F");
  
  lcd.clrScr();
  lcd.update();
  
  //lightIntensity = analogRead(LDRpin);
  
  //lightIntensity = map(lightIntensity, 1050, 20, 0, 100);

  str = String(h)+"%";
  str.toCharArray(lightString,4);
  
  lcd.print("HUMIDITY",CENTER,0);
  lcd.print(lightString,CENTER,20); 
  
  printBar();
  fillBar(h);
  
  lcd.update();  
  delay(3000);
  
  lcd.clrScr();
  lcd.update();
  
  //lightIntensity = analogRead(LDRpin);
  
  //lightIntensity = map(lightIntensity, 1050, 20, 0, 100);

  str = String(t)+"*C";
  str.toCharArray(lightString,4);
  
  lcd.print("TEMPERATURE",CENTER,0);
  lcd.print(lightString,CENTER,20); 
  
  printBar();
  fillBar(t);
  
  lcd.update();  
  delay(3000);

}

void printBar()
{  
  lcd.drawRect(2, 35, 81, 40);
  lcd.update(); 
}

void fillBar(int percent)
{
    percent = map(percent,0,100,2,81);     
    lcd.drawLine(2, 36, percent, 36);
    lcd.drawLine(2, 37, percent, 37);
    lcd.drawLine(2, 38, percent, 38);
    lcd.drawLine(2, 39, percent, 39);
}

