void setup() {
  // put your setup code here, to run once:
  pinMode(9,OUTPUT);//Motor Driver Pin 1
  pinMode(10,OUTPUT);//Motor Driver Pin 2
  pinMode(11,OUTPUT);//Motor Driver Pin 3
  pinMode(12,OUTPUT);//Motor Driver Pin 4

  pinMode(6,INPUT);//Left Ir Sensor Signal Input
  pinMode(7,INPUT);//Right Ir Sensor Signal Input
  
}

void loop() {
  // put your main code here, to run repeatedly:
  int r =digitalRead(7);
  int l=digitalRead(6);
  if(r == HIGH){
    
    digitalWrite(11,HIGH);
    digitalWrite(12,LOW);
    digitalWrite(9,HIGH);
    digitalWrite(10,LOW);
  }else if(l==HIGH){
   
    digitalWrite(12,HIGH);
    digitalWrite(11,LOW);
    digitalWrite(10,HIGH);
    digitalWrite(9,LOW);
  }else if(r == LOW && l == LOW){
    digitalWrite(11,HIGH);
    digitalWrite(12,LOW);
    digitalWrite(10,HIGH);
    digitalWrite(9,LOW);
    
  }else if(l == HIGH && r==HIGH){
      digitalWrite(12,HIGH);
      digitalWrite(11,LOW);
      digitalWrite(9,HIGH);
      digitalWrite(10,LOW);
      if(r == HIGH){
    
        digitalWrite(11,HIGH);
        digitalWrite(12,LOW);
        digitalWrite(9,HIGH);
        digitalWrite(10,LOW);
      }else if(l==HIGH){
       
        digitalWrite(12,HIGH);
        digitalWrite(11,LOW);
        digitalWrite(10,HIGH);
        digitalWrite(9,LOW);
      }else if(r == LOW && l == LOW){
        digitalWrite(11,HIGH);
        digitalWrite(12,LOW);
        digitalWrite(10,HIGH);
        digitalWrite(9,LOW);
        
        }

  }
  }
