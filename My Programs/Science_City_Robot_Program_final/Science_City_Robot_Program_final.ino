
#define m11 10  // rear motor
#define m12 11
#define m21 12    // front motor
#define m22 13
#define mc 3
#define ml 4
#define mtl 5
#define mtr 6
//#include "DHT.h"
/*
//DHT dht;
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#define BMP_SCK A5
#define BMP_SDI A4
//#define BMP_MOSI 11 
//#define BMP_CS 10

Adafruit_BMP280 bmp; // I2C
//Adafruit_BMP280 bmp(BMP_CS); // hardware SPI
//Adafruit_BMP280 bmp(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);
*/
char str[2],i;
void Catch()
{
  digitalWrite(mc,HIGH);
  digitalWrite(ml,LOW);

}
void Leave()
{
  digitalWrite(mc,LOW);
  digitalWrite(ml,HIGH);

}
void Tright()
{
  digitalWrite(mtr,HIGH);
  digitalWrite(mtl,LOW);

}
void Tleft()
{
  digitalWrite(mtl,HIGH);
  digitalWrite(mtr,LOW); 
}
void forward()
{
  
   digitalWrite(m11, HIGH);
   digitalWrite(m12, LOW);
   digitalWrite(m21, HIGH);
   digitalWrite(m22, LOW);
}
void backward()
{
   digitalWrite(m11, LOW);
   digitalWrite(m12, HIGH);
   digitalWrite(m21, LOW);
   digitalWrite(m22, HIGH); 
}
void left()
{
   digitalWrite(m11, HIGH);
   digitalWrite(m12, LOW);
   delay(100);
   digitalWrite(m21, LOW);
   digitalWrite(m22, HIGH);
}
void right()
{
   digitalWrite(m11, LOW);
   digitalWrite(m12, HIGH);
   delay(100);
   digitalWrite(m21, HIGH);
   digitalWrite(m22, LOW);
}
void Stop()
{
   digitalWrite(m11, LOW);
   digitalWrite(m12, LOW);
   digitalWrite(m21, LOW);
   digitalWrite(m22, LOW);
   digitalWrite(mtr,LOW);
   digitalWrite(mtl,LOW);
   digitalWrite(mc,LOW);
   digitalWrite(ml,LOW);
}
/*void sensorbmp()
{
   Serial.print(F("   Temperature = "));
    Serial.print(bmp.readTemperature());
    Serial.println(" *C");
    
    Serial.print(F("   Pressure = "));
    Serial.print(bmp.readPressure()/3389.39*25.4);
    Serial.println("   mm/Hg");

    Serial.print(F("   Approx altitude = "));
    Serial.print(bmp.readAltitude(1013.25)); // this should be adjusted to your local forcase
    Serial.println(" m");
    
    Serial.println();


}*/
void setup() 
{
  
  delay(5000);
    Serial.begin(9600);
  Serial.println("Bluetooth Controlled Robot!");
  Serial.println("Made By- Vimarsh Shah");
  Serial.begin(9600);
  pinMode(m11, OUTPUT);
  pinMode(m12, OUTPUT);
  pinMode(m21, OUTPUT);
  pinMode(m22, OUTPUT);
  pinMode(mc, OUTPUT);
  pinMode(ml, OUTPUT);
  pinMode(mtr, OUTPUT);
  pinMode(mtl, OUTPUT);
 /*  Serial.println(F("   BMP280 test"));
  
  if (!bmp.begin()) {  
    Serial.println(F("   Could not find a valid BMP280 sensor, check wiring!"));
    while (1);*/
  }
/*Serial.begin(9600);
  Serial.println();
  Serial.println("Status\tHumidity (%)\tTemperature (C)\t(F)");

  dht.setup(9); // data pin 2*/

void loop() 
{
  while(Serial.available())
  {
    char ch=Serial.read();
    str[i++]=ch;
   
    if(str[i-1]=='1')
    {
    Serial.println("Forward");
     forward();
     i=0;
    }
    else if(str[i-1]=='2')
    {
    Serial.println("Left");
     right();
     i=0;
    }
    else if(str[i-1]=='3')
    {
    Serial.println("Right");
      left();
      i=0;
    }
    
    else if(str[i-1]=='4')
    {
    Serial.println("Backward");
      backward();
      i=0;
    }
    else if(str[i-1]=='5')
    {
    Serial.println("Stop");
      Stop();
      i=0;

    }
   else if(str[i-1]=='7')
   {
   Serial.println("Catch");
   Catch();
   i=0;
   }
   else if(str[i-1]=='8')
   {
   Serial.println("Leave");
   Leave();
   i=0;                  
   }
   else if(str[i-1]=='6')
   {
   Serial.println("Arm Moving Down");
   Tleft();
   i=0;
   }
   else if(str[i-1]=='9')
   {
   Serial.println("Arm Moving Up");
   Tright();
   i=0;
   }
    
    
    delay(100);
  }
 // sensorbmp();
 /* delay(dht.getMinimumSamplingPeriod());

  float humidity = dht.getHumidity();
  float temperature = dht.getTemperature();

  Serial.print(dht.getStatusString());
  Serial.print("\t");
  Serial.print(humidity, 1);
  Serial.print("\t\t");
  Serial.print(temperature, 1);
  Serial.print("\t\t");
  Serial.println(dht.toFahrenheit(temperature), 1);*/


}
