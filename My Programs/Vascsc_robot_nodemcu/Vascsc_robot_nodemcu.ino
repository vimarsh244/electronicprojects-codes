


/* Comment this out to disable prints and save space */
#define BLYNK_PRINT Serial
#define m11 D1
#define m12 D2
#define m21 D3
#define m22 D4

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "01a9ff2f43754a398f1f2c7ade651e5d";

// Your WiFi cred entials.
// Set password to "" for open networks.
char ssid[] = "ESP@vimarsh";
char pass[] = "password@ESP";
void setup()
{
  // Debug console
  Serial.begin(9600);
  pinMode(m11, OUTPUT);
  pinMode(m12, OUTPUT);
  pinMode(m21, OUTPUT);
  pinMode(m22, OUTPUT);
  Blynk.begin(auth, ssid, pass);
Blynk.email("Subject: VASCSC", "My Blynk project is online!");

  // You can also specify server:
  //Blynk.begin(auth, ssid, pass, "blynk-cloud.com", 8442);
  //Blynk.begin(auth, ssid, pass, IPAddress(192,168,1,100), 8442);
}

void loop()
{
  Blynk.run();

  // You can inject your own code or combine it with other sketches.
  // Check other examples on how to communicate with Blynk. Remember
  // to avoid delay() function!
}
 BLYNK_WRITE(V1) 
{
  int x = param[0].asInt();
  int y = param[1].asInt();

  // Do something with x and y
/*  Serial.print("X = ");
  Serial.print(x);
  Serial.print("; Y = ");
  Serial.println(y);*/
  if(y>220)
  right();
  else if(y<35)
  left();
  else if(x>220)
  backward();
  else if(x<35)
  forward();
  else
  Stop();
}
void forward()
{
  digitalWrite(m11, HIGH);
  digitalWrite(m12,LOW);
  digitalWrite(m21,HIGH);
  digitalWrite(m22,LOW);
}

void backward()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12,HIGH);
  digitalWrite(m21,LOW);
  digitalWrite(m22,HIGH);
}

void right()
{
  digitalWrite(m11, HIGH);
  digitalWrite(m12,LOW);
  digitalWrite(m21,LOW);
  digitalWrite(m22,HIGH);
}

void left()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12,HIGH);
  digitalWrite(m21,HIGH);
  digitalWrite(m22,LOW);
}

void Stop()
{
  digitalWrite(m11, LOW);
  digitalWrite(m12,LOW);
  digitalWrite(m21,LOW);
  digitalWrite(m22,LOW);
}
