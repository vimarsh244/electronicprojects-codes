#define BLYNK_PRINT Serial

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>


Adafruit_BMP280 bmp; // I2C

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "583cf486e4aa4fbaacd4a1fd084aa9db";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Vardhman_2.4GHz";
char pass[] = "V$9825067189$";

// This function sends Arduino's up time every second to Virtual Pin (5).
// In the app, Widget's reading frequency should be set to PUSH. This means
BlynkTimer timer;
// that you define how often to send data to Blynk App.
void bmpsens() {
  float t = bmp.readTemperature();
  float p = bmp.readPressure();
  float a = (bmp.readAltitude(1013.25));
  Blynk.virtualWrite(V1, t);
  Blynk.virtualWrite(V2, p);
  Blynk.virtualWrite(V3, a);
}

void setup()
{
  // Debug console
  Serial.begin(9600);

  Blynk.begin(auth, ssid, pass);
 /* if (!bmp.begin()) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }
  */
  // You can also specify server:
  //Blynk.begin(auth, ssid, pass, "blynk-cloud.com", 8442);
  //Blynk.begin(auth, ssid, pass, IPAddress(192,168,1,100), 8442);
  timer.setInterval(5000L, bmpsens);
}

void loop()
{
  Blynk.run();
  // Initiates BlynkTimer
  timer.run();
//  bmpsens();
}



