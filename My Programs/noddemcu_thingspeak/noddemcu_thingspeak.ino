#include <DHT.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ThingSpeak.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>


Adafruit_BMP280 bmp; // I2C

const char* ssid = "Vardhman_2.4GHz";
const char* password = "V$9825067189$";
WiFiClient client;
unsigned long myChannelNumber = 432689;
const char * myWriteAPIKey = "1F9L003PZ2IBGZBM";
float temperature, pressure;
void setup()
{
  Serial.begin(115200);
  if (!bmp.begin()) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }
  delay(10);
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  // Print the IP address
  Serial.println(WiFi.localIP());
  ThingSpeak.begin(client);
}
void loop()
{
  temperature = (bmp.readTemperature());
  pressure = ((bmp.readPressure()) * 0.00750062);
  Serial.print("Temperature Value is :");
  Serial.print(temperature);
  Serial.println("C");
  Serial.print("pressure Value is :");
  Serial.print(pressure);
  Serial.println("mm/Hg");
  // Write to ThingSpeak. There are up to 8 fields in a channel, allowing you to store up to 8 different
  // pieces of information in a channel. Here, we write to field 1.

  ThingSpeak.writeField(myChannelNumber, 1, temperature, myWriteAPIKey);

  ThingSpeak.writeField(myChannelNumber, 2, pressure, myWriteAPIKey);

  delay(300000); // ThingSpeak will only accept updates every 15 seconds.
}


