// Robo India Tutorial  
// Simple code upload the tempeature and humidity data using thingspeak.com
// Hardware: NodeMCU,DHT11

#include <ESP8266WiFi.h>

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>


Adafruit_BMP280 bmp; // I2C
//Adafruit_BMP280 bmp(BMP_CS); // hardware SPI
//Adafruit_BMP280 bmp(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);

String apiKey = "1F9L003PZ2IBGZBM";     //  Enter your Write API key from ThingSpeak

const char *ssid =  "Vardhman_2.4GHz";     // replace with your wifi ssid and wpa2 key
const char *pass =  "V$9825067189$";
const char* server = "api.thingspeak.com";


WiFiClient client;

void setup()
{
  Serial.begin(115200);
  delay(10);


  Serial.println("Connecting to ");
  Serial.println(ssid);


  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  if (!bmp.begin()) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }


}

void loop()
{

  float t = (bmp.readTemperature());
  float p = ((bmp.readPressure()) * 0.00750062);
  if (client.connect(server, 80))  //   "184.106.153.149" or api.thingspeak.com
  {

    String postStr = apiKey;
    postStr += "&field1=";
    postStr += String(t);
    postStr += "&field2=";
    postStr += String(p);
    postStr += "\r\n\r\n";

    client.print("POST /update HTTPS/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);

    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.print(" degrees Celcius, Pressure:: ");
    Serial.print(p);
    Serial.println("mm/Hg. Send to Thingspeak.");
  }
  client.stop();

  Serial.println("going to deepsleeep...");

  delay(300000);
}

