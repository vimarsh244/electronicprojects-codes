const int VAL_PROBE = 0; // Analog pin 0
const int MOISTURE_LEVEL = 300; // the value after the LED goes ON


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void LedState(int state) {
    digitalWrite(13, state);
}

void loop() {
  // put your main code here, to run repeatedly:
  int moisture = analogRead(VAL_PROBE);
 
    Serial.println(moisture);
 
    if(moisture > MOISTURE_LEVEL) {
        LedState(HIGH);
    } else   {
        LedState(LOW);
    }
    delay(100);
}

/*
 * const int VAL_PROBE = 0; // Analog pin 0
const int MOISTURE_LEVEL = 250; // the value after the LED goes ON
 
void setup() {
    Serial.begin(9600);
}
 
void LedState(int state) {
    digitalWrite(13, state);
}
 
void loop() {
    int moisture = analogRead(VAL_PROBE);
 
    Serial.println(moisture);
 
    if(moisture > MOISTURE_LEVEL) {
        LedState(HIGH);
    } else   {
        LedState(LOW);
    }
    delay(100);
}
 */
